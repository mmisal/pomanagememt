package com.pom.pomanagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {
    private static final int SPLASH_TIME_OUT = 1000;
    private Timer splashTimer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        runThread();
    }

    private void runThread() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this,
                        RegistrationAndLoginActivity.class);
                startActivity(intent);
                finish();
            }
        };
        splashTimer.schedule(timerTask, SPLASH_TIME_OUT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        splashTimer.cancel();
    }
}