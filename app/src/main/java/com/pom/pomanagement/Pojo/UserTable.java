package com.pom.pomanagement.pojo;

import java.io.Serializable;

/**
 * Created by Mayur Misal  15/10/2018
 */
public class UserTable implements Serializable, Cloneable {
    private Long userId = null, userMode = null;
    private int userStatus = -1;
    private String userFullName = null, userMobileNumber = null,
            userPassword = null, userEmailId = null, userAddress = null,
            userGCMKey = null, userImagePath = null;

    public UserTable() {
        super();
    }

    public UserTable(Long userId, long userMode,
                     int userStatus,
                     String userFullName, String userMobileNumber,
                     String userPassword, String userEmailId,
                     String userAddress, String userGCMKey,
                     String userImagePath) {
        this.userId = userId;
        this.userMode = userMode;
        this.userStatus = userStatus;
        this.userFullName = userFullName;
        this.userMobileNumber = userMobileNumber;
        this.userPassword = userPassword;
        this.userEmailId = userEmailId;
        this.userAddress = userAddress;
        this.userGCMKey = userGCMKey;
        this.userImagePath = userImagePath;
    }

    public UserTable(long userMode,
                     int userStatus,
                     String userFullName, String userMobileNumber,
                     String userPassword, String userEmailId,
                     String userAddress, String userGCMKey) {
        this.userId = null;
        this.userMode = userMode;
        this.userStatus = userStatus;
        this.userFullName = userFullName;
        this.userMobileNumber = userMobileNumber;
        this.userPassword = userPassword;
        this.userEmailId = userEmailId;
        this.userAddress = userAddress;
        this.userGCMKey = userGCMKey;
        this.userImagePath = null;
    }

    public static enum STATUS {
        ACTIVE, DE_ACTIVE
    }

    public static enum MODE {
        ADMIN, CLIENT, VENDOR,
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserMode() {
        return userMode;
    }

    public void setUserMode(long userMode) {
        this.userMode = userMode;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserGCMKey() {
        return userGCMKey;
    }

    public void setUserGCMKey(String userGCMKey) {
        this.userGCMKey = userGCMKey;
    }

    public String getUserImagePath() {
        return userImagePath;
    }

    public void setUserImagePath(String userImagePath) {
        this.userImagePath = userImagePath;
    }
}