package com.pom.pomanagement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pom.pomanagement.pojo.UserTable;
import com.pom.pomanagement.utility.Utility;
import com.pom.pomanagement.webService.AddUser;
import com.pom.pomanagement.webService.abstractClasses.IRequest;
import com.pom.pomanagement.webService.abstractClasses.Ordinals;

import java.util.ArrayList;

public class RegistrationAndLoginActivity extends AppCompatActivity implements View
        .OnClickListener, IRequest {
    private LinearLayout registrationLayout = null;
    private Spinner spnUserType = null;
    private EditText edtFullName = null, edtMobileNumber = null,
            edtPassword = null, edtConfirmPassword = null, edtEmailId = null,
            edtAddress = null;
    private Button btnRegister = null;

    private LinearLayout loginLayout = null;
    private EditText edtUserMobileNumber = null,
            edtUserPassword = null;
    private TextView forgotPassword = null;
    private Button btnLogin = null, btnRegisterMe = null;

    private LinearLayout forgotPasswordLayout = null;
    private EditText edtForgottenUserMobileNumber = null,
            edtForgottenUserEmailId = null;
    private Button btnShowPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_and_login);

        initializeRegistration();
        initializeLogin();
        initializeForgottenPassword();

        registrationLayout.setVisibility(View.GONE);
        loginLayout.setVisibility(View.VISIBLE);
        forgotPasswordLayout.setVisibility(View.GONE);
    }

    private void initializeRegistration() {
        registrationLayout = findViewById(R.id.registrationLayout);
        spnUserType = findViewById(R.id.spnUserType);
        edtFullName = findViewById(R.id.edtFullName);
        edtMobileNumber = findViewById(R.id.edtMobileNumber);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtEmailId = findViewById(R.id.edtEmailId);
        edtAddress = findViewById(R.id.edtAddress);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        ArrayList<String> modeArray = new ArrayList();
        for(UserTable.MODE mode: UserTable.MODE.values()){
            modeArray.add(mode.toString());
        }
        modeArray.add(0, getString(R.string.selectUserMode));
        ArrayAdapter<String> userModeArray = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                modeArray);
        userModeArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnUserType.setAdapter(userModeArray);
    }

    private void initializeLogin() {
        loginLayout = findViewById(R.id.loginLayout);
        edtUserMobileNumber = findViewById(R.id.edtUserMobileNumber);
        edtUserPassword = findViewById(R.id.edtUserPassword);
        forgotPassword = findViewById(R.id.forgotPassword);
        btnRegisterMe = findViewById(R.id.btnRegisterMe);
        btnRegisterMe.setOnClickListener(this);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    private void initializeForgottenPassword() {
        forgotPasswordLayout = findViewById(R.id.forgotPasswordLayout);
        edtForgottenUserEmailId = findViewById(R.id.edtForgottenUserEmailId);
        edtForgottenUserMobileNumber = findViewById(R.id.edtForgottenUserMobileNumber);
        btnShowPassword = findViewById(R.id.btnShowPassword);
        btnShowPassword.setOnClickListener(this);
    }

    private boolean validUserBeforeRegister() {
        if (Utility.isNullString(edtFullName.getText().toString().trim())) {
            edtFullName.setError(getString(R.string.enterValidFullName));
            edtFullName.requestFocus();
            return true;
        }

        if (Utility.isNullString(edtMobileNumber.getText().toString().trim())
                ||
                edtMobileNumber.getText().toString().trim().length() != 10) {
            edtMobileNumber.setError(getString(R.string.enterValidMobileNumber));
            edtMobileNumber.requestFocus();
            return true;
        }

        if (Utility.isNullString(edtPassword.getText().toString().trim())) {
            edtPassword.setError(getString(R.string.enterValidPassword));
            edtPassword.requestFocus();
            return true;
        }

        if (Utility.isNullString(edtConfirmPassword.getText().toString().trim())) {
            edtConfirmPassword.setError(getString(R.string.enterValidPassword));
            edtConfirmPassword.requestFocus();
            return true;
        }

        if (!edtPassword.getText().toString().trim()
                .equals(
                        edtConfirmPassword
                                .getText()
                                .toString()
                                .trim())) {
            edtConfirmPassword.setError(getString(R.string.passwordDoesNotMatch));
            edtConfirmPassword.requestFocus();
            return true;
        }

        if (Utility.isNullString(edtEmailId.getText().toString().trim())) {
            edtEmailId.setError(getString(R.string.enterValidEmailId));
            edtEmailId.requestFocus();
            return true;
        }

        return false;
    }

    private boolean validUserBeforeLogin() {
        if (Utility.isNullString(edtUserMobileNumber.getText().toString().trim())) {
            edtUserMobileNumber.setError(getString(R.string.enterValidMobileNumber));
            edtUserMobileNumber.requestFocus();
            return true;
        }

        if (Utility.isNullString(edtUserPassword.getText().toString().trim())) {
            edtUserPassword.setError(getString(R.string.enterValidPassword));
            edtUserPassword.requestFocus();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister: {
                if (validUserBeforeRegister() == Boolean.TRUE) {
                    Log.e(Utility.TAG, "Registration fail! need to all fields here.");
                    return;
                }

                UserTable registerUser = new UserTable(
                        UserTable.MODE.ADMIN.ordinal(),
                        UserTable.STATUS.ACTIVE.ordinal(),
                        edtFullName.getText().toString().trim(),
                        edtMobileNumber.getText().toString().trim(),
                        edtPassword.getText().toString().trim(),
                        edtEmailId.getText().toString().trim(),
                        edtAddress.getText().toString().trim(),
                        null);

                AddUser addUser = new AddUser(this, Ordinals.REGISTER_USER.ordinal(), registerUser);
                addUser.fireRequest();
            }
            break;

            case R.id.btnRegisterMe: {
                registrationLayout.setVisibility(View.VISIBLE);
                forgotPasswordLayout.setVisibility(View.GONE);
                loginLayout.setVisibility(View.GONE);
            }
            break;

            case R.id.btnLogin: {
                if (validUserBeforeLogin() == Boolean.TRUE) {
                    Log.e(Utility.TAG, "Login fail! need to all field here.");
                    return;
                }

                ArrayList<UserTable> userTables = Utility.getUserDetailList(this);
                if (userTables == null) {
                    Toast.makeText(this,
                            getString(R.string.userNotFound),
                            Toast.LENGTH_LONG).show();
                    registrationLayout.setVisibility(View.VISIBLE);
                    loginLayout.setVisibility(View.GONE);
                    forgotPasswordLayout.setVisibility(View.GONE);
                    return;
                }

                for (UserTable user : userTables) {
                    if (user.getUserMobileNumber().equals(
                            edtUserMobileNumber.getText().toString().trim())
                            &&
                            user.getUserPassword().equals(
                                    edtUserPassword.getText().toString().trim())) {
                        Log.i(Utility.TAG, "Login successful! welcome " + user
                                .getUserFullName());
                        Intent homeIntent = new Intent
                                (RegistrationAndLoginActivity.this, MainActivity.class);
                        startActivity(homeIntent);
                        break;
                    }
                }

                Toast.makeText(this,
                        getString(R.string.userNotFound),
                        Toast.LENGTH_LONG).show();
                registrationLayout.setVisibility(View.VISIBLE);
                loginLayout.setVisibility(View.GONE);
                forgotPasswordLayout.setVisibility(View.GONE);
            }
            break;

            case R.id.btnShowPassword: {
            }
            break;

            case R.id.forgotPassword: {
                registrationLayout.setVisibility(View.GONE);
                forgotPasswordLayout.setVisibility(View.VISIBLE);
                loginLayout.setVisibility(View.GONE);
            }
            break;
        }
    }

    @Override
    public void onBackPressed() {
        if (loginLayout.getVisibility() == View.VISIBLE) {
            finish();
            return;
        }

        if (registrationLayout.getVisibility() == View.VISIBLE
                ||
                forgotPasswordLayout.getVisibility() == View.VISIBLE) {
            loginLayout.setVisibility(View.VISIBLE);
            forgotPasswordLayout.setVisibility(View.GONE);
            registrationLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void iOnRequestCompleted(IRequest requestedActivity, int operationIdentifier, Object response) {
        switch (Ordinals.values()[operationIdentifier]) {
            case REGISTER_USER: {
                Intent homeIntent = new Intent
                        (RegistrationAndLoginActivity.this, MainActivity.class);
                startActivity(homeIntent);
            }
            break;
        }
    }
// 2) VendorTable
//       --vendorId(long), userId(long), creditId(long), isCreditApplied(int), companyName
// (varchar256), companyAddress(varchar256). vendorStatus(int)
//3) ProductTable
//-- productId(long), productName(varchar256), productInformation(varchar256),
//           productRate(varchar256), productUnit(int), productStatus(int), productPhotoPath
// (varchar256),
//            4) CreditTable
//-- CreditId(long), POID(long), CreditLimit(varchar256), CreditWithdrawal(varchar256), CreditDeposit(varchar256), CreditStatus(int),
//            5) PaymentTable
//-- PaymentId(long), UserId(long), POId(long), PaymentDate(long), PaymentInfo(varchar256), Discount(varchar256),
//    DiscountPercentage(int), TaxPercentage(int), TaxAmount(varchar256),
//7) POTable
//-- POID(long), userid(long), CompanyId(long), ProudctId(long), PODate(long), POStatus(int), isCreditUsed(int),PayId(long),
//            8) NotificationTable,
//            -- NotificationId(long), NotificationTitle(varchar256), NotificationMessage(varchar256), NotificationDateTime(long),
//    NotificationStatus(int)
//9) UserHasNotificationTable,
//            --UserHasNotificationId(long), UserId(long), NotificationId(long), UserHasNotificationStatus(int)

}